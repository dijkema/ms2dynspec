#!/usr/bin/env python3

from os.path import basename
from glob import glob
import sys
import os
import casacore.tables as pt
import numpy as np
import h5py
import shutil

from argparse import ArgumentParser
from write_dynspec_hdf5 import write_hdf5
import subprocess

def extract_median_autocorrelations(input_ms_list, full_pol=True):
    """
    Extract median IQUV of autocorrelations from all measurement sets saved in input_directory.
    Saves in a casacore table in a new subdirectory of the working directory.

    Returns the name of the subdirectory.
    """
    first_ms = input_ms_list[0]
    obsname = basename(first_ms).split("_")[0]

    os.mkdir(obsname + "_tmp")

    if full_pol:
        stokes = 'IQUV'
    else:
        stokes = 'I'

    taql_processes = []
    for input_ms in input_ms_list:
        output_table = obsname + "_tmp" + "/" + basename(input_ms).replace(".MS", ".tab")
        taql_cmd = f"select GAGGR(medians(abs(mscal.stokes(DATA[2:-2,], '{stokes}')), 0)) as STOKES from {input_ms} where ANTENNA1==ANTENNA2 GROUPBY ANTENNA1 giving {output_table} as plain"
        taql_processes.append(subprocess.Popen(["taql", "-m", "0", "-nopr", taql_cmd]))

    for taql_process in taql_processes:
        taql_process.wait()
        assert(taql_process.returncode == 0)

    print("All taql done")
    return obsname + "_tmp", obsname

def combine_bands(tmp_directory, input_ms_list):
    tmp_table_list = sorted(glob(f"{tmp_directory}/*.tab"))
    first_ms = input_ms_list[0]

    assert len(input_ms_list) == len(tmp_table_list)

    test_table = pt.table(tmp_table_list[0], ack=False).getcol("STOKES")
    num_stations, num_times, num_corr = test_table.shape

    num_subbands = len(tmp_table_list)

    total_data = np.zeros([num_subbands, num_stations, num_times, num_corr])

    for i, tmp_table in enumerate(tmp_table_list):
        iquv = pt.table(tmp_table, ack=False).getcol("STOKES")
        total_data[i] = iquv

    return total_data



if __name__ == "__main__":
    parser = ArgumentParser(description="Convert directory with measurement sets into DynSpec HDF5")
    parser.add_argument("-iquv", "--iquv", help="Store all Stokes parameters", action='store_true')
    parser.add_argument("-sapid", "--sapid", help="sapid of observation", type=int, default=0)
    parser.add_argument("ms_path", help="Directory containing subbands")

    args = parser.parse_args()

    ms_list = sorted(glob(args.ms_path + f"/*SAP{args.sapid:03d}*.MS"))
    if len(ms_list)<=0:
        ms_list = sorted(glob(args.ms_path + f"/*.MS"))
    
    tmp_dir, obsname = extract_median_autocorrelations(ms_list, full_pol=args.iquv)
    data = combine_bands(tmp_dir, ms_list)
    write_hdf5(ms_list, data, obsname, args.sapid)
    shutil.rmtree(tmp_dir)
    
