import numpy as np
import h5py
import copy
from astropy.time import Time
import casacore.tables as pt


def_attrs = {
 'ANTENNA_SET' : 'LBA_OUTER',
 'BF_FORMAT' : 'TAB',
 'BF_VERSION' : '',
 'CHANNELS_PER_SUBANDS' : 1,
 'CHANNEL_WIDTH' : 0.1953125,
 'CHANNEL_WIDTH_UNIT' : 'MHz',
 'CLOCK_FREQUENCY' : 200.0,
 'CLOCK_FREQUENCY_UNIT' : 'MHz',
 'CREATE_OFFLINE_ONLINE' : 'Offline',
 'DOC_NAME' : 'ICD 6:Dynamic-Spectrum Data',
 'DOC_VERSION' : 'ICD6:v-2.03.10',
 'DYNSPEC_GROUP' : 1,
 'FILEDATE' : '',
 'FILENAME' : '',
 'FILETYPE' : 'dynspec',
 'FILTER_SELECTION' : 'LBA_10_90',
 'GROUPETYPE' : 'Root',
 'NOF_DYNSPEC' : 114,
 'NOF_SAMPLES' : 240230,
 'NOF_TILED_DYNSPEC' : 0,
 'NOTES' : '',
 'OBSERVATION_END_MJD' : 59373.775010922,
 'OBSERVATION_END_UTC' : '2021-06-08T19:17:59.937843084Z - 0 s',
 'OBSERVATION_FREQUENCY_CENTER' : 44.4305419921875,
 'OBSERVATION_FREQUENCY_MAX' : 63.96484375,
 'OBSERVATION_FREQUENCY_MIN' : 24.896240234375,
 'OBSERVATION_FREQUENCY_UNIT' : 'Mhz',
 'OBSERVATION_ID' : '818854',
 'OBSERVATION_NOF_BITS_PER_SAMPLE' : 8,
 'OBSERVATION_NOF_STATIONS' : 38,
 'OBSERVATION_START_MJD' : 59373.775,
 'OBSERVATION_START_UTC' : '2021-06-08T18:36:00.000000000Z + 0 s',
 'OBSERVATION_STATIONS_LIST' : ['CS001LBA', 'CS002LBA', 'CS003LBA', 'CS004LBA', 'CS005LBA', 'CS006LBA', 'CS007LBA', 'CS011LBA', 'CS013LBA', 'CS017LBA', 'CS021LBA', 'CS024LBA', 'CS026LBA', 'CS028LBA', 'CS030LBA', 'CS031LBA', 'CS032LBA', 'CS101LBA', 'CS103LBA', 'CS201LBA', 'CS301LBA', 'CS302LBA', 'CS401LBA', 'CS501LBA', 'RS106LBA', 'RS205LBA', 'RS208LBA', 'RS210LBA', 'RS305LBA', 'RS306LBA', 'RS307LBA', 'RS310LBA', 'RS406LBA', 'RS407LBA', 'RS409LBA', 'RS503LBA', 'RS508LBA', 'RS509LBA'],
 'PIPELINE_NAME' : 'DYNAMIC SPECTRUM',
 'PIPELINE_VERSION' : 'Dynspec v.2.0',
    'POINT_ALTITUDE' : [],
    'POINT_AZIMUTH' : [],
 'POINT_DEC' : 58.81500000000001,
 'POINT_RA' : 350.85,
 'PRIMARY_POINTING_DIAMETER' : 0.0,
 'PROJECT_CONTACT' : '',
 'PROJECT_CO_I' : '',
 'PROJECT_ID' : '',
 'PROJECT_PI' : '',
 'PROJECT_TITLE' : 'Long Term LOFAR Observations of the mid Latitude ionosphere',
 'SAMPLING_RATE' : 9.5367431640625e-05,
 'SAMPLING_RATE_UNIT' : 'Mhz',
 'SAMPLING_TIME' : 10485.76,
 'SAMPLING_TIME_UNIT' : 'μs',
 'SUBBAND_WIDTH' : 0.1953125,
 'SUBBAND_WIDTH_UNIT' : 'Mhz',
 'SYSTEM_TEMPERATURE' : [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
 'SYSTEM_VERSION' : 'Unknown',
 'TARGET' : 'CasA',
 'TELESCOPE' : 'LOFAR',
 'TOTAL_BAND_WIDTH' : 39.0625,
 'TOTAL_INTEGRATION_TIME' : 2518.994140625,
 'TOTAL_INTEGRATION_TIME_UNIT' : 's',
 'WHEATER_HUMIDITY' : np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
    'WHEATER_STATIONS_LIST' : [], 
 'WHEATER_TEMPERATURE' : np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
}


def_dynspec_attrs = {
 'BARYCENTER' : 0,
 'BEAM_DIAMETER' : 0.0,
 'BEAM_DIAMETER_DEC' : 0.0,
 'BEAM_DIAMETER_RA' : 0.0,
 'BEAM_FREQUENCY_CENTER' : 44.427489,
 'BEAM_FREQUENCY_MAX' : 63.861084,
 'BEAM_FREQUENCY_MIN' : 24.993894,
 'BEAM_FREQUENCY_UNIT' : 'Mhz',
 'BEAM_NOF_STATIONS' : 1.0,
 'BEAM_STATIONS_LIST' : np.array(['CS001LBA']),
 'COMPLEX_VOLTAGE' : 0,
 'DEDISPERSION' : 'NONE',
 'DISPERSION_MEASURE' : 0.0,
 'DISPERSION_MEASURE_UNIT' : '',
 'DYNSPEC_BANDWIDTH' : 39.0625,
 'DYNSPEC_START_MJD' : '59373.8',
 'DYNSPEC_START_UTC' : '2021-06-08T18:36:00.000000000Z + 0 s',
 'DYNSPEC_STOP_MJD' : '59373.859373.8',
 'DYNSPEC_STOP_UTC' : '2021-06-08T19:17:59.937843084Z - 0 s',
 'GROUPETYPE' : 'DYNSPEC',
 'ONOFF' : 'ON',
 'POINT_DEC' : 0.0,
 'POINT_RA' : 0.0,
 'POSITION_OFFSET_DEC' : -58.81500000000001,
 'POSITION_OFFSET_RA' : -350.85,
 'SIGNAL_SUM' : 'COHERENT',
 'STOCKES_COMPONENT' : ['I'],
 'TARGET' : 'CasA',
 'TRACKING' : 'J2000',
}


def_coordinates_attrs = {
    'GROUPE_TYPE' : 'Coordinates',
    'REF_LOCATION_FRAME' : 'ITRF',
    'TRACKING_LOCATION' : [3826577.066,  461022.948, 5064892.786],
    'REF_TIME_FRAME' : 'MJD',
    'REF_TIME_UNIT' : 'd',
}


observation_attrs = { #translation of some Dynspec attrs names to MS info
    'ANTENNA_SET': ['OBSERVATION', 'LOFAR_ANTENNA_SET'],
    'CLOCK_FREQUENCY': ['OBSERVATION', 'LOFAR_CLOCK_FREQUENCY'],
    'FILEDATE': ['OBSERVATION', 'LOFAR_FILEDATE'],
    'FILENAME': ['OBSERVATION', 'LOFAR_FILENAME'],
    'FILETYPE': ['OBSERVATION', 'LOFAR_FILETYPE'],
    'FILTER_SELECTION': ['OBSERVATION', 'LOFAR_FILTER_SELECTION'],
    'OBSERVATION_FREQUENCY_CENTER': ['OBSERVATION',
                                     'LOFAR_OBSERVATION_FREQUENCY_CENTER'],
    'OBSERVATION_FREQUENCY_MAX': ['OBSERVATION',
                                  'LOFAR_OBSERVATION_FREQUENCY_MAX'],
    'OBSERVATION_FREQUENCY_MIN': ['OBSERVATION',
                                  'LOFAR_OBSERVATION_FREQUENCY_MIN'],
    'OBSERVATION_ID': ['OBSERVATION', 'LOFAR_OBSERVATION_ID'],
    'OBSERVATION_NOF_BITS_PER_SAMPLE': ['OBSERVATION',
                                        'LOFAR_NOF_BITS_PER_SAMPLE'],
    'PIPELINE_NAME': ['OBSERVATION', 'LOFAR_PIPELINE_NAME'],
    'PIPELINE_VERSION': ['OBSERVATION', 'LOFAR_PIPELINE_VERSION'],
    'PROJECT_CONTACT': ['OBSERVATION', 'LOFAR_PROJECT_CONTACT'],
    'PROJECT_CO_I': ['OBSERVATION', 'LOFAR_PROJECT_CO_I'],
    'PROJECT_PI': ['OBSERVATION', 'LOFAR_PROJECT_PI'],
    'PROJECT_TITLE': ['OBSERVATION', 'LOFAR_PROJECT_TITLE'],
    'SYSTEM_VERSION': ['OBSERVATION', 'LOFAR_SYSTEM_VERSION'],
    'TARGET': ['OBSERVATION', 'LOFAR_TARGET'],
    'TELESCOPE': ['OBSERVATION', 'TELESCOPE_NAME']
}


def get_observation_attrs(mydict, MS):
    for key,value in observation_attrs.items():
        sub, colname = value[0], value[1]
        val = pt.table(f"{MS}/{sub}",ack=False).getcol(colname)
        if type(val) == dict:
            mydict[ key ] = val[ 'array' ][0]
        else:
            mydict[ key ] = val[0]

def get_station_names(ms):
    t = pt.table(ms + "/ANTENNA", ack=False)
    return t.getcol("NAME"), t.getcol("POSITION")

def get_subband_frequencies(ms_list):
    frequencies = []
    for ms in ms_list:
        ms_freq = pt.table(ms + "/SPECTRAL_WINDOW", ack=False).getcol("REF_FREQUENCY")[0]
        frequencies.append(ms_freq)

    assert np.all(np.diff(frequencies) > 0)  # Ascending
    return frequencies

def get_start_stop(ms):
    """ Get start and stop time in mjd (float) and UTC (str)"""
    # TODO (TAQL or python-casacore) with astropy? or no astropy, format string with python-casacore (challenge)
    start_mjd = pt.table(f"{ms}/OBSERVATION",ack=False).getcol("LOFAR_OBSERVATION_START")[0]/(3600*24) 
    stop_mjd = pt.table(f"{ms}/OBSERVATION",ack=False).getcol("LOFAR_OBSERVATION_END")[0]/(3600*24)
    start_utc = Time(start_mjd, format="mjd").isot
    stop_utc = Time(stop_mjd, format="mjd").isot
    return start_mjd, stop_mjd, start_utc, stop_utc
    
            
def get_attrs_from_ms(MS):
    mydict = copy.deepcopy(def_attrs)
    get_observation_attrs(mydict, MS)
    station_names, station_pos = get_station_names(MS)
    mydict['OBSERVATION_STATIONS_LIST'] = station_names
    mydict['OBSERVATION_STATIONS_POSITION'] = station_pos
    mydict['OBSERVATION_NOF_STATIONS'] = len(station_names)

    start_mjd, stop_mjd, start_utc, stop_utc = get_start_stop(MS)
    mydict['OBSERVATION_START_MJD'] = start_mjd
    mydict['OBSERVATION_START_UTC'] = start_utc
    mydict['OBSERVATION_END_MJD'] = stop_mjd
    mydict['OBSERVATION_END_UTC'] = stop_utc
    mydict['TOTAL_INTEGRATION_TIME'] = ( stop_mjd - start_mjd )* ( 3600 * 24 )
    mydict['TOTAL_INTEGRATION_TIME_UNIT'] = 's'
    
    

    mydict['CHANNELS_PER_SUBANDS'] = pt.table(f"{MS}/SPECTRAL_WINDOW", ack=False ).getcol("NUM_CHAN")[0]
    mydict['CHANNEL_WIDTH'] =  pt.table(f"{MS}/SPECTRAL_WINDOW", ack=False ).getcol("CHAN_WIDTH")[0][0]/1.e6
    mydict['SUBBAND_WIDTH'] = pt.table(f"{MS}/SPECTRAL_WINDOW", ack=False ).getcol("TOTAL_BANDWIDTH")[0]/1e6

    radec = pt.table(f"{MS}/FIELD",ack=False).getcol("PHASE_DIR")[0][0] 
    mydict['POINT_DEC'] = np.degrees(radec[1])
    mydict['POINT_RA'] = np.degrees(radec[0])

    return mydict


def get_attrs_dict(ms_list, num_times, num_stations, num_subbands):
    mydict = get_attrs_from_ms(ms_list[0])
    mydict['NOF_DYNSPEC'] = num_stations,
    mydict['NOF_SAMPLES'] = num_times,
    assert mydict['OBSERVATION_NOF_STATIONS'] == num_stations
    subband_frequencies = get_subband_frequencies(ms_list)
    assert len(subband_frequencies) == num_subbands
    
    
    mydict['TOTAL_BAND_WIDTH'] = subband_frequencies[ -1 ] * 1e-6 - subband_frequencies[ 0 ] * 1e-6

    mydict['SAMPLING_TIME'] =  mydict['TOTAL_INTEGRATION_TIME'] / num_times
    mydict['SAMPLING_TIME_UNIT'] = mydict['TOTAL_INTEGRATION_TIME_UNIT'] 
    mydict['SAMPLING_RATE'] = 1./  mydict['SAMPLING_TIME']
    mydict['SAMPLING_RATE_UNIT'] = 'Hz' #should be extracted from SAMPLING_TIME_UNIT
    mydict['SUBBAND_FREQUENCIES'] = subband_frequencies
    return mydict

    
def write_hdf5(ms_list, data, obsname, sapid=0):
    num_subbands, num_stations, num_times, num_corr = data.shape
    mydict = get_attrs_dict(ms_list, num_times, num_stations, num_subbands)

    with h5py.File(f"Dynspec_rebinned_{obsname}_SAP{sapid:03d}.h5", "w") as h5:
        h5.attrs.update(mydict)
        station_names = mydict['OBSERVATION_STATIONS_LIST']
        station_positions = mydict['OBSERVATION_STATIONS_POSITION']
        for station_num, station in enumerate(station_names): 
            grp = h5.create_group(f"DYNSPEC_{station_num:03d}")
            dynspec_dict = copy.deepcopy(def_dynspec_attrs)
            
            dynspec_dict['TARGET'] = mydict['TARGET']
            dynspec_dict['BEAM_STATIONS_LIST'] = [station]

            dynspec_dict['DYNSPEC_START_MJD'] = mydict['OBSERVATION_START_MJD']
            dynspec_dict['DYNSPEC_STOP_MJD'] = mydict['OBSERVATION_END_MJD']
            dynspec_dict['DYNSPEC_START_UTC'] = mydict['OBSERVATION_START_UTC']
            dynspec_dict['DYNSPEC_STOP_UTC'] = mydict['OBSERVATION_END_UTC']

            grp.attrs.update(dynspec_dict)
            
            coordinates_grp = grp.create_group("COORDINATES")
            coordinates_grp.attrs['COORDINATE_TYPES'] = ["Time", "Spectral", "Polarization"]
            coordinates_grp.attrs["NOF_AXIS"] = 3
            coordinates_grp.attrs["REF_LOCATION_FRAME"] = "ITRF"
            coordinates_grp.attrs["REF_LOCATION_UNIT"] = ["m", "m", "m"]
            coordinates_grp.attrs["REF_LOCATION_VALUE"] = station_positions[station_num]
            
            coordinates_grp.attrs.update(def_coordinates_attrs)
            
            pol_grp = coordinates_grp.create_group("POLARIZATION")
            if num_corr == 1:
                pol_grp.attrs["AXIS_NAMES"] = "I"
            else:
                pol_grp.attrs["AXIS_NAMES"] = ["I", "Q", "U", "V"]

            spectral_grp = coordinates_grp.create_group("SPECTRAL")
            spectral_grp.attrs["AXIS_NAMES"] = "Frequency"
            spectral_grp.attrs["AXIS_UNIT"] = "Hz"
            spectral_grp.attrs["AXIS_VALUE_WORLD"] = mydict["SUBBAND_FREQUENCIES"]

            time_grp = coordinates_grp.create_group("TIME")
            time_grp.attrs["AXIS_NAMES"] = "Time"
            time_grp.attrs["AXIS_UNIT"] = [mydict['SAMPLING_TIME_UNIT']]
            time_grp.attrs["COORDINATE_TYPE"] = "Time"
            time_grp.attrs["GROUPE_TYPE"] = "TimeCoord"
            time_grp.attrs["INCREMENT"] = [ mydict['SAMPLING_TIME']]
            time_grp.attrs["NOF_AXES"] = 1
            time_grp.attrs["PC"] = [1. 0.]
            time_grp.attrs["REFERENCE_PIXEL"] = [0.]
            time_grp.attrs["REFERENCE_VALUE"] = [0.]
            time_grp.attrs["STORAGE_TYPE"] = ['Linear']

            dataset = grp.create_dataset("DATA", (num_times, num_subbands, num_corr))
            dataset[:] = data[:, station_num, :, :].transpose(1, 0, 2)


